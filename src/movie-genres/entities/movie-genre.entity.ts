/* eslint-disable prettier/prettier */
import { Movie } from 'src/movies/entities/movie.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Genre } from 'src/genres/entities/genre.entity';

@Entity()
export class MovieGenre {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Movie, (movie) => movie.movieGenres)
  movie: Movie;

  @ManyToOne(() => Genre, (genre) => genre.movieGenres)
  genre: Genre;
}
