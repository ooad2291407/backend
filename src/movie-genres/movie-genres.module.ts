/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MovieGenresService } from './movie-genres.service';
import { MovieGenresController } from './movie-genres.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovieGenre } from './entities/movie-genre.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MovieGenre])],
  controllers: [MovieGenresController],
  providers: [MovieGenresService],
})
export class MovieGenresModule {}
