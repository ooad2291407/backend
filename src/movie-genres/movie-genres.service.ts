/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { MovieGenre } from './entities/movie-genre.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMovieGenreDto } from './dto/create-movie-genre.dto';
import { UpdateMovieGenreDto } from './dto/update-movie-genre.dto';

@Injectable()
export class MovieGenresService {
  constructor(
    @InjectRepository(MovieGenre)
    private movieGenresRepository: Repository<MovieGenre>,
  ) {}

  create(createMovieGenreDto: CreateMovieGenreDto) {
    return this.movieGenresRepository.save(createMovieGenreDto);
  }

  findAll() {
    return this.movieGenresRepository.find();
  }

  findOne(id: number) {
    return this.movieGenresRepository.findOne({ where: { id } });
  }

  async update(id: number, updateMovieGenreDto: UpdateMovieGenreDto) {
    const movieGenre = await this.movieGenresRepository.findOneBy({ id });
    if (!movieGenre) {
      throw new NotFoundException();
    }
    const updatedMovieGenre = { ...movieGenre, ...updateMovieGenreDto };

    return this.movieGenresRepository.save(updatedMovieGenre);
  }

  async remove(id: number) {
    const movieGenre = await this.movieGenresRepository.findOneBy({ id });
    if (!movieGenre) {
      throw new NotFoundException();
    }
    return this.movieGenresRepository.remove(movieGenre);
  }
}
