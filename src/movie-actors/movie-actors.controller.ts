/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MovieActorsService } from './movie-actors.service';
import { CreateMovieActorDto } from './dto/create-movie-actor.dto';
import { UpdateMovieActorDto } from './dto/update-movie-actor.dto';

@Controller('movie-actors')
export class MovieActorsController {
  constructor(private readonly movieActorsService: MovieActorsService) {}

  @Post()
  create(@Body() createMovieActorDto: CreateMovieActorDto) {
    return this.movieActorsService.create(createMovieActorDto);
  }

  @Get()
  findAll() {
    return this.movieActorsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.movieActorsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMovieActorDto: UpdateMovieActorDto,
  ) {
    return this.movieActorsService.update(+id, updateMovieActorDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.movieActorsService.remove(+id);
  }
}
