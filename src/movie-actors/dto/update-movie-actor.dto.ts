/* eslint-disable prettier/prettier */
import { PartialType } from '@nestjs/mapped-types';
import { CreateMovieActorDto } from './create-movie-actor.dto';

export class UpdateMovieActorDto extends PartialType(CreateMovieActorDto) {}
