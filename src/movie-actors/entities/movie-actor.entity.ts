/* eslint-disable prettier/prettier */
import { Actor } from 'src/actors/entities/actor.entity';
import { Movie } from 'src/movies/entities/movie.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class MovieActor {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Movie, (movie) => movie.movieActors)
  movie: Movie;

  @ManyToOne(() => Actor, (actor) => actor.movieActors)
  actor: Actor;
}
