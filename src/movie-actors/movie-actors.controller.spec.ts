/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { MovieActorsController } from './movie-actors.controller';
import { MovieActorsService } from './movie-actors.service';

describe('MovieActorsController', () => {
  let controller: MovieActorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MovieActorsController],
      providers: [MovieActorsService],
    }).compile();

    controller = module.get<MovieActorsController>(MovieActorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
