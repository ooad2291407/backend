/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MovieActorsService } from './movie-actors.service';
import { MovieActorsController } from './movie-actors.controller';
import { MovieActor } from './entities/movie-actor.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([MovieActor])],
  controllers: [MovieActorsController],
  providers: [MovieActorsService],
})
export class MovieActorsModule {}
