/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { MovieActorsService } from './movie-actors.service';

describe('MovieActorsService', () => {
  let service: MovieActorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MovieActorsService],
    }).compile();

    service = module.get<MovieActorsService>(MovieActorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
