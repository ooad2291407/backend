/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { MovieActor } from './entities/movie-actor.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMovieActorDto } from './dto/create-movie-actor.dto';
import { UpdateMovieActorDto } from './dto/update-movie-actor.dto';

@Injectable()
export class MovieActorsService {
  constructor(
    @InjectRepository(MovieActor)
    private movieActorsRepository: Repository<MovieActor>,
  ) {}

  create(createMovieActorDto: CreateMovieActorDto) {
    return this.movieActorsRepository.save(createMovieActorDto);
  }

  findAll() {
    return this.movieActorsRepository.find();
  }

  findOne(id: number) {
    return this.movieActorsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateMovieActorDto: UpdateMovieActorDto) {
    const movieActor = await this.movieActorsRepository.findOneBy({ id });
    if (!movieActor) {
      throw new NotFoundException();
    }
    const updatedMovieActor = { ...movieActor, ...updateMovieActorDto };

    return this.movieActorsRepository.save(updatedMovieActor);
  }

  async remove(id: number) {
    const movieActor = await this.movieActorsRepository.findOneBy({ id });
    if (!movieActor) {
      throw new NotFoundException();
    }
    return this.movieActorsRepository.remove(movieActor);
  }
}
