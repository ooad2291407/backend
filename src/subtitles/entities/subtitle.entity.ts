/* eslint-disable prettier/prettier */
import { Language } from 'src/languages/entities/language.entity';
import { Movie } from 'src/movies/entities/movie.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Subtitle {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Movie, (movie) => movie.subtitles)
  movie: Movie;

  @ManyToOne(() => Language, (language) => language.subtitles)
  language: Language;
}
