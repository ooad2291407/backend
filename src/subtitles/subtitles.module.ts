/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { SubtitlesService } from './subtitles.service';
import { SubtitlesController } from './subtitles.controller';
import { Subtitle } from './entities/subtitle.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Subtitle])],
  controllers: [SubtitlesController],
  providers: [SubtitlesService],
})
export class SubtitlesModule {}
