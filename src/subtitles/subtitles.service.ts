/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateSubtitleDto } from './dto/create-subtitle.dto';
import { UpdateSubtitleDto } from './dto/update-subtitle.dto';
import { Subtitle } from './entities/subtitle.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SubtitlesService {
  constructor(
    @InjectRepository(Subtitle)
    private subtitlesRepository: Repository<Subtitle>,
  ) {}

  create(createSubtitleDto: CreateSubtitleDto) {
    return this.subtitlesRepository.save(createSubtitleDto);
  }

  findAll() {
    return this.subtitlesRepository.find();
  }

  findOne(id: number) {
    return this.subtitlesRepository.findOne({ where: { id } });
  }

  async update(id: number, updateSubtitleDto: UpdateSubtitleDto) {
    const subtitle = await this.subtitlesRepository.findOneBy({ id });
    if (!subtitle) {
      throw new NotFoundException();
    }
    const updatedSubtitle = { ...subtitle, ...updateSubtitleDto };

    return this.subtitlesRepository.save(updatedSubtitle);
  }

  async remove(id: number) {
    const subtitle = await this.subtitlesRepository.findOneBy({ id });
    if (!subtitle) {
      throw new NotFoundException();
    }
    return this.subtitlesRepository.remove(subtitle);
  }
}
