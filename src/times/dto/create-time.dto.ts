/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class CreateTimeDto {
  @IsNotEmpty()
  time: number;
}
