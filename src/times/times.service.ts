/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Time } from './entities/time.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TimesService {
  constructor(
    @InjectRepository(Time)
    private timesRepository: Repository<Time>,
  ) { }

  findAll() {
    return this.timesRepository.find();
  }

  findOne(id: number) {
    return this.timesRepository.findOne({ where: { id } });
  }

}
