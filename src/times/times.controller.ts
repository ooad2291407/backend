/* eslint-disable prettier/prettier */
import { Controller, Get, Param } from '@nestjs/common';
import { TimesService } from './times.service';

@Controller('times')
export class TimesController {
  constructor(private readonly timesService: TimesService) {}

  @Get()
  findAll() {
    return this.timesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.timesService.findOne(+id);
  }
}
