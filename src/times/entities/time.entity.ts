/* eslint-disable prettier/prettier */
import { ShowTime } from 'src/show-times/entities/show-time.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Time {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  time: string;

  @OneToMany(() => ShowTime, (showTime) => showTime.time)
  showTimes: ShowTime[];
}
