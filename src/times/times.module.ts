/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TimesService } from './times.service';
import { TimesController } from './times.controller';
import { Time } from './entities/time.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Time])],
  controllers: [TimesController],
  providers: [TimesService],
})
export class TimesModule {}
