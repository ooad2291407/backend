/* eslint-disable prettier/prettier */
import { IsNotEmpty, MinLength, Matches } from 'class-validator';
export class CreateUserDto {
  image = 'no_img.jpg';
  @IsNotEmpty()
  @MinLength(5)
  email: string;

  @IsNotEmpty()
  first_name: string;

  @IsNotEmpty()
  last_name: string;

  @IsNotEmpty()
  @MinLength(10)
  phone: string;

  @IsNotEmpty()
  rank: string;

  @Matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/)
  @IsNotEmpty()
  @MinLength(5)
  password: string;
}
