/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { OccupiesService } from './occupies.service';

describe('OccupiesService', () => {
  let service: OccupiesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OccupiesService],
    }).compile();

    service = module.get<OccupiesService>(OccupiesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
