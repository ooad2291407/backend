/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { OccupiesController } from './occupies.controller';
import { OccupiesService } from './occupies.service';

describe('OccupiesController', () => {
  let controller: OccupiesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OccupiesController],
      providers: [OccupiesService],
    }).compile();

    controller = module.get<OccupiesController>(OccupiesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
