/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { OccupiesService } from './occupies.service';
import { OccupiesController } from './occupies.controller';
import { Occupy } from './entities/occupy.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Occupy])],
  controllers: [OccupiesController],
  providers: [OccupiesService],
})
export class OccupiesModule {}
