/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OccupiesService } from './occupies.service';
import { CreateOccupyDto } from './dto/create-occupy.dto';
import { UpdateOccupyDto } from './dto/update-occupy.dto';

@Controller('occupies')
export class OccupiesController {
  constructor(private readonly occupiesService: OccupiesService) {}

  @Post()
  create(@Body() createOccupyDto: CreateOccupyDto) {
    return this.occupiesService.create(createOccupyDto);
  }

  @Get()
  findAll() {
    return this.occupiesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.occupiesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateOccupyDto: UpdateOccupyDto) {
    return this.occupiesService.update(+id, updateOccupyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.occupiesService.remove(+id);
  }
}
