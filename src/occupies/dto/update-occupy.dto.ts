/* eslint-disable prettier/prettier */
import { PartialType } from '@nestjs/mapped-types';
import { CreateOccupyDto } from './create-occupy.dto';

export class UpdateOccupyDto extends PartialType(CreateOccupyDto) {}
