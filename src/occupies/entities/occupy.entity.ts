/* eslint-disable prettier/prettier */
import { Seat } from 'src/seats/entities/seat.entity';
import { ShowTime } from 'src/show-times/entities/show-time.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

@Entity()
export class Occupy {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  status: string;

  @ManyToOne(() => ShowTime, (showTime) => showTime.occupies)
  showTime: ShowTime[];

  @ManyToOne(() => Seat, (seat) => seat.occupies)
  seat: Seat[];
}
