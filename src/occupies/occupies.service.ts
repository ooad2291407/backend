/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOccupyDto } from './dto/create-occupy.dto';
import { UpdateOccupyDto } from './dto/update-occupy.dto';
import { Occupy } from './entities/occupy.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class OccupiesService {
  constructor(
    @InjectRepository(Occupy)
    private occupiesRepository: Repository<Occupy>,
  ) {}

  create(createOccupyDto: CreateOccupyDto) {
    return this.occupiesRepository.save(createOccupyDto);
  }

  findAll() {
    return this.occupiesRepository.find();
  }

  findOne(id: number) {
    return this.occupiesRepository.findOne({ where: { id } });
  }

  async update(id: number, updateOccupyDto: UpdateOccupyDto) {
    const occupy = await this.occupiesRepository.findOneBy({ id });
    if (!occupy) {
      throw new NotFoundException();
    }
    const updatedOccupy = { ...occupy, ...updateOccupyDto };

    return this.occupiesRepository.save(updatedOccupy);
  }

  async remove(id: number) {
    const occupy = await this.occupiesRepository.findOneBy({ id });
    if (!occupy) {
      throw new NotFoundException();
    }
    return this.occupiesRepository.remove(occupy);
  }
}
