/* eslint-disable prettier/prettier */
import { Movie } from 'src/movies/entities/movie.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Director {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Movie, (movie) => movie.director)
  movies: Movie[];
}
