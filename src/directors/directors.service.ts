/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Director } from 'src/directors/entities/director.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DirectorsService {
  constructor(
    @InjectRepository(Director)
    private directorsRepository: Repository<Director>,
  ) {}

  findAll() {
    return this.directorsRepository.find();
  }

  findOne(id: number) {
    return this.directorsRepository.findOne({ where: { id } });
  }
  
}
