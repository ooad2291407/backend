/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ShowTimesService } from './show-times.service';
import { CreateShowTimeDto } from './dto/create-show-time.dto';
import { UpdateShowTimeDto } from './dto/update-show-time.dto';

@Controller('show-times')
export class ShowTimesController {
  constructor(private readonly showTimesService: ShowTimesService) {}

  @Post()
  create(@Body() createShowTimeDto: CreateShowTimeDto) {
    return this.showTimesService.create(createShowTimeDto);
  }

  @Get()
  findAll() {
    return this.showTimesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.showTimesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateShowTimeDto: UpdateShowTimeDto,
  ) {
    return this.showTimesService.update(+id, updateShowTimeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.showTimesService.remove(+id);
  }
}
