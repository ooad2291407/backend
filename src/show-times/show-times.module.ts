/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { ShowTimesService } from './show-times.service';
import { ShowTimesController } from './show-times.controller';
import { ShowTime } from './entities/show-time.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ShowTime])],
  controllers: [ShowTimesController],
  providers: [ShowTimesService],
})
export class ShowTimesModule {}
