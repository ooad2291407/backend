/* eslint-disable prettier/prettier */
import { Booking } from 'src/bookings/entities/booking.entity';
import { Movie } from 'src/movies/entities/movie.entity';
import { Time } from 'src/times/entities/time.entity';
import { Screen } from 'src/screens/entities/screen.entity';
import { Occupy } from 'src/occupies/entities/occupy.entity';
import { Date } from 'src/dates/entities/date.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';

@Entity()
export class ShowTime {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Time, (time) => time.showTimes)
  time: Time;

  @ManyToOne(() => Movie, (movie) => movie.showTimes)
  movie: Movie;

  @ManyToOne(() => Screen, (screen) => screen.showTimes)
  screen: Screen;

  @OneToMany(() => Booking, (booking) => booking.showTime)
  bookings: Booking[];

  @OneToMany(() => Occupy, (occupy) => occupy.showTime)
  occupies: Occupy;

  @ManyToOne(() => Date, (date) => date.showTimes)
  date: Date;
}
