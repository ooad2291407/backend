/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateShowTimeDto } from './dto/create-show-time.dto';
import { UpdateShowTimeDto } from './dto/update-show-time.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ShowTime } from './entities/show-time.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ShowTimesService {
  constructor(
    @InjectRepository(ShowTime)
    private showTimesRepository: Repository<ShowTime>,
  ) {}

  create(createShowTimeDto: CreateShowTimeDto) {
    return this.showTimesRepository.save(createShowTimeDto);
  }

  findAll() {
    return this.showTimesRepository.find();
  }

  findOne(id: number) {
    return this.showTimesRepository.findOne({ where: { id } });
  }

  async update(id: number, updateShowTimeDto: UpdateShowTimeDto) {
    const showTime = await this.showTimesRepository.findOneBy({ id });
    if (!showTime) {
      throw new NotFoundException();
    }
    const updatedShowTime = { ...showTime, ...updateShowTimeDto };

    return this.showTimesRepository.save(updatedShowTime);
  }

  async remove(id: number) {
    const showTime = await this.showTimesRepository.findOneBy({ id });
    if (!showTime) {
      throw new NotFoundException();
    }
    return this.showTimesRepository.remove(showTime);
  }
}
