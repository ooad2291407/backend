/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
import { Invoice } from './entities/invoice.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from 'src/users/entities/user.entity';
import { Seat } from 'src/seats/entities/seat.entity';
import { Booking } from 'src/bookings/entities/booking.entity';
import { Ticket } from 'src/tickets/entities/ticket.entity';

@Injectable()
export class InvoicesService {
  constructor(
    @InjectRepository(Invoice)
    private invoicesRepository: Repository<Invoice>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Seat)
    private seatsRepository: Repository<Seat>,
    @InjectRepository(Booking)
    private bookingsRepository: Repository<Booking>,
    @InjectRepository(Ticket)
    private ticketsRepository: Repository<Ticket>,
  ) {}

  async create(createInvoiceDto: CreateInvoiceDto) {
    const user = await this.usersRepository.findOneBy({
      id: createInvoiceDto.userId,
    });
    const invoice: Invoice = new Invoice();
    invoice.user = user;
    invoice.price = 0;
    await this.invoicesRepository.save(invoice);

    for (const iv of createInvoiceDto.bookings) {
      const booking = new Booking();
      booking.price = iv.price;
      booking.seat = await this.seatsRepository.findOneBy({
        id: iv.seatId,
      });
      booking.price = booking.seat.price;
      booking.invoice = invoice;

      await this.bookingsRepository.save(booking);
      invoice.price = invoice.price + booking.price;
    }
    await this.invoicesRepository.save(invoice);
    const ticket: Ticket = new Ticket();
    ticket.user = user;
    ticket.invoice = invoice;
    await this.ticketsRepository.save(ticket);
    await this.ticketsRepository.save(ticket);

    return await this.invoicesRepository.findOne({
      where: { id: invoice.id },
      relations: ['bookings'],
    });
  }

  findAll() {
    return this.invoicesRepository.find();
  }

  findOne(id: number) {
    return this.invoicesRepository.findOne({ where: { id } });
  }

  // async update(id: number, updateInvoiceDto: UpdateInvoiceDto) {
  //   const invoice = await this.invoicesRepository.findOneBy({ id });
  //   if (!invoice) {
  //     throw new NotFoundException();
  //   }
  //   const updatedInvoice = { ...invoice, ...updateInvoiceDto };

  //   return this.invoicesRepository.save(updatedInvoice);
  // }

  async remove(id: number) {
    const invoice = await this.invoicesRepository.findOneBy({ id });
    if (!invoice) {
      throw new NotFoundException();
    }
    return this.invoicesRepository.remove(invoice);
  }
}
