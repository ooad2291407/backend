/* eslint-disable prettier/prettier */
import { Booking } from 'src/bookings/entities/booking.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Invoice {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  price: number;

  @Column()
  discount: number;

  @Column()
  total_price: number;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(() => Promotion, (promotion) => promotion.invoices)
  promotion: Promotion;

  @OneToMany(() => Booking, (booking) => booking.invoice)
  bookings: Booking;

  @ManyToOne(() => User, (user) => user.invoices)
  user: User;
}
