/* eslint-disable prettier/prettier */
import { CreateBookingDto } from 'src/bookings/dto/create-booking.dto';

export class CreateInvoiceDto {
  userId: number;
  bookings: CreateBookingDto[];
}
