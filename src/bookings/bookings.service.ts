/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { UpdateBookingDto } from './dto/update-booking.dto';
import { Booking } from './entities/booking.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BookingsService {
  constructor(
    @InjectRepository(Booking)
    private bookingsRepository: Repository<Booking>,
  ) {}

  findAll() {
    return this.bookingsRepository.find({ relations: ['seat'] });
  }

  findOne(id: number) {
    return this.bookingsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateBookingDto: UpdateBookingDto) {
    const booking = await this.bookingsRepository.findOneBy({ id });
    if (!booking) {
      throw new NotFoundException();
    }
    const updatedBooking = { ...booking, ...updateBookingDto };

    return this.bookingsRepository.save(updatedBooking);
  }

  async remove(id: number) {
    const booking = await this.bookingsRepository.findOneBy({ id });
    if (!booking) {
      throw new NotFoundException();
    }
    return this.bookingsRepository.remove(booking);
  }
}
