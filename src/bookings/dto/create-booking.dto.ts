/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class CreateBookingDto {
  @IsNotEmpty()
  seatId: number;

  @IsNotEmpty()
  price: number;
}
