/* eslint-disable prettier/prettier */
import { Invoice } from 'src/invoices/entities/invoice.entity';
import { Seat } from 'src/seats/entities/seat.entity';
import { ShowTime } from 'src/show-times/entities/show-time.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Booking {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  price: number;

  @ManyToOne(() => Seat, (seat) => seat.bookings)
  seat: Seat;

  @ManyToOne(() => ShowTime, (showTime) => showTime.bookings)
  showTime: ShowTime;

  @ManyToOne(() => Invoice, (invoice) => invoice.bookings)
  invoice: Invoice;
}
