/* eslint-disable prettier/prettier */
import { ShowTime } from 'src/show-times/entities/show-time.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Date {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @OneToMany(() => ShowTime, (showTime) => showTime.date)
  showTimes: ShowTime[];
}
