/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Date } from './entities/date.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class DatesService {
  constructor(
    @InjectRepository(Date)
    private datesRepository: Repository<Date>,
  ) { }

  findAll() {
    return this.datesRepository.find();
  }

  findOne(id: number) {
    return this.datesRepository.findOne({ where: { id } });
  }
}
