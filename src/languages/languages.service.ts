/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Language } from './entities/language.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LanguagesService {
  constructor(
    @InjectRepository(Language)
    private languagesRepository: Repository<Language>,
  ) { }

  findAll() {
    return this.languagesRepository.find();
  }

  findOne(id: number) {
    return this.languagesRepository.findOne({ where: { id } });
  }
}
