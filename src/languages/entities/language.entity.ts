/* eslint-disable prettier/prettier */
import { Subtitle } from 'src/subtitles/entities/subtitle.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Language {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  language: string;

  @OneToMany(() => Subtitle, (subtitle) => subtitle.language)
  subtitles: Subtitle[];
}
