/* eslint-disable prettier/prettier */
import { Hall } from 'src/halls/entities/hall.entity';
import { ShowTime } from 'src/show-times/entities/show-time.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Screen {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => ShowTime, (showTime) => showTime.screen)
  showTimes: ShowTime[];

  @ManyToOne(() => Hall, (hall) => hall.screens)
  hall: Hall;
}
