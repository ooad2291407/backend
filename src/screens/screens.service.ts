/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Screen } from './entities/screen.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ScreensService {
  constructor(
    @InjectRepository(Screen)
    private screensRepository: Repository<Screen>,
  ) {}

  findAll() {
    return this.screensRepository.find({ relations: ['hall'] });
  }

  findOne(id: number) {
    return this.screensRepository.findOne({ where: { id } });
  }
}
