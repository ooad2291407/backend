/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class CreatePromotionDto {
  @IsNotEmpty()
  discount: number;

  @IsNotEmpty()
  outdated_date: string;
}
