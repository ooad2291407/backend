/* eslint-disable prettier/prettier */
import { Invoice } from 'src/invoices/entities/invoice.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  discount: number;

  @Column()
  outdated_date: Date;

  @CreateDateColumn()
  createdAt: Date;

  @OneToMany(() => Invoice, (invoice) => invoice.promotion)
  invoices: Invoice[];
}
