/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Genre } from './entities/genre.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class GenresService {
  constructor(
    @InjectRepository(Genre)
    private genresRepository: Repository<Genre>,
  ) { }

  findAll() {
    return this.genresRepository.find();
  }

  findOne(id: number) {
    return this.genresRepository.findOne({ where: { id } });
  }
}
