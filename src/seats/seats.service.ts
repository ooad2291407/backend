/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Seat } from './entities/seat.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SeatsService {
  constructor(
    @InjectRepository(Seat)
    private seatsRepository: Repository<Seat>,
  ) {}
  findAll() {
    return this.seatsRepository.find();
  }

  findOne(id: number) {
    return this.seatsRepository.findOne({ where: { id } });
  }
}
