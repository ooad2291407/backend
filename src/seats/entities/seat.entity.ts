/* eslint-disable prettier/prettier */
import { Booking } from 'src/bookings/entities/booking.entity';
import { HallSeat } from 'src/hall-seats/entities/hall-seat.entity';
import { Occupy } from 'src/occupies/entities/occupy.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Seat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float' })
  price: number;

  @OneToMany(() => Booking, (booking) => booking.seat)
  bookings: Booking[];

  @OneToMany(() => Occupy, (occupy) => occupy.seat)
  occupies: Occupy[];

  @OneToMany(() => HallSeat, (hallSeat) => hallSeat.seat)
  hallSeats: HallSeat[];
}
