/* eslint-disable prettier/prettier */
import { Hall } from 'src/halls/entities/hall.entity';
import { Movie } from 'src/movies/entities/movie.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Program {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Movie, (movie) => movie.programs)
  movie: Movie;

  @ManyToOne(() => Hall, (hall) => hall.programs)
  hall: Hall;
}
