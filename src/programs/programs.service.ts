/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProgramDto } from './dto/create-program.dto';
import { UpdateProgramDto } from './dto/update-program.dto';
import { Program } from './entities/program.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProgramsService {
  constructor(
    @InjectRepository(Program)
    private programsRepository: Repository<Program>,
  ) {}

  create(createProgramDto: CreateProgramDto) {
    return this.programsRepository.save(createProgramDto);
  }

  findAll() {
    return this.programsRepository.find();
  }

  findOne(id: number) {
    return this.programsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateProgramDto: UpdateProgramDto) {
    const program = await this.programsRepository.findOneBy({ id });
    if (!program) {
      throw new NotFoundException();
    }
    const updatedProgram = { ...program, ...updateProgramDto };

    return this.programsRepository.save(updatedProgram);
  }

  async remove(id: number) {
    const program = await this.programsRepository.findOneBy({ id });
    if (!program) {
      throw new NotFoundException();
    }
    return this.programsRepository.remove(program);
  }
}
