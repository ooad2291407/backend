/* eslint-disable prettier/prettier */
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { HallSeatsService } from './hall-seats.service';
import { CreateHallSeatDto } from './dto/create-hall-seat.dto';
import { UpdateHallSeatDto } from './dto/update-hall-seat.dto';

@Controller('hall-seats')
export class HallSeatsController {
  constructor(private readonly hallSeatsService: HallSeatsService) {}

  @Post()
  create(@Body() createHallSeatDto: CreateHallSeatDto) {
    return this.hallSeatsService.create(createHallSeatDto);
  }

  @Get()
  findAll() {
    return this.hallSeatsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.hallSeatsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateHallSeatDto: UpdateHallSeatDto,
  ) {
    return this.hallSeatsService.update(+id, updateHallSeatDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.hallSeatsService.remove(+id);
  }
}
