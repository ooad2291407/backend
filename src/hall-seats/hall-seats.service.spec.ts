/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { HallSeatsService } from './hall-seats.service';

describe('HallSeatsService', () => {
  let service: HallSeatsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HallSeatsService],
    }).compile();

    service = module.get<HallSeatsService>(HallSeatsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
