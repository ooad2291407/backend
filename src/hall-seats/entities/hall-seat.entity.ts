/* eslint-disable prettier/prettier */
import { Hall } from 'src/halls/entities/hall.entity';
import { Seat } from 'src/seats/entities/seat.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class HallSeat {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Hall, (hall) => hall.hallSeats)
  hall: Hall;

  @ManyToOne(() => Seat, (seat) => seat.hallSeats)
  seat: Hall;
}
