/* eslint-disable prettier/prettier */
import { PartialType } from '@nestjs/mapped-types';
import { CreateHallSeatDto } from './create-hall-seat.dto';

export class UpdateHallSeatDto extends PartialType(CreateHallSeatDto) {}
