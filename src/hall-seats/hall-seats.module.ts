/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { HallSeatsService } from './hall-seats.service';
import { HallSeatsController } from './hall-seats.controller';
import { HallSeat } from './entities/hall-seat.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([HallSeat])],
  controllers: [HallSeatsController],
  providers: [HallSeatsService],
})
export class HallSeatsModule {}
