/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateHallSeatDto } from './dto/create-hall-seat.dto';
import { UpdateHallSeatDto } from './dto/update-hall-seat.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { HallSeat } from './entities/hall-seat.entity';
import { Repository } from 'typeorm';

@Injectable()
export class HallSeatsService {
  constructor(
    @InjectRepository(HallSeat)
    private hallSeatsRepository: Repository<HallSeat>,
  ) {}

  create(createHallSeatDto: CreateHallSeatDto) {
    return this.hallSeatsRepository.save(createHallSeatDto);
  }

  findAll() {
    return this.hallSeatsRepository.find();
  }

  findOne(id: number) {
    return this.hallSeatsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateHallSeatDto: UpdateHallSeatDto) {
    const hallSeat = await this.hallSeatsRepository.findOneBy({ id });
    if (!hallSeat) {
      throw new NotFoundException();
    }
    const updatedHallSeat = { ...hallSeat, ...updateHallSeatDto };

    return this.hallSeatsRepository.save(updatedHallSeat);
  }

  async remove(id: number) {
    const hallSeat = await this.hallSeatsRepository.findOneBy({ id });
    if (!hallSeat) {
      throw new NotFoundException();
    }
    return this.hallSeatsRepository.remove(hallSeat);
  }
}
