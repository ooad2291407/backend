/* eslint-disable prettier/prettier */
import { Test, TestingModule } from '@nestjs/testing';
import { HallSeatsController } from './hall-seats.controller';
import { HallSeatsService } from './hall-seats.service';

describe('HallSeatsController', () => {
  let controller: HallSeatsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HallSeatsController],
      providers: [HallSeatsService],
    }).compile();

    controller = module.get<HallSeatsController>(HallSeatsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
