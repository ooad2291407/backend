/* eslint-disable prettier/prettier */
import { Controller, Get, Param } from '@nestjs/common';
import { HallsService } from './halls.service';


@Controller('halls')
export class HallsController {
  constructor(private readonly hallsService: HallsService) { }

  @Get()
  findAll() {
    return this.hallsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.hallsService.findOne(+id);
  }
}
