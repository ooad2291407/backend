/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Hall } from './entities/hall.entity';

@Injectable()
export class HallsService {
  constructor(
    @InjectRepository(Hall)
    private hallsRepository: Repository<Hall>,
  ) {}

  findAll() {
    return this.hallsRepository.find();
  }

  findOne(id: number) {
    return this.hallsRepository.findOne({ where: { id } });
  }
}
