/* eslint-disable prettier/prettier */
import { HallSeat } from 'src/hall-seats/entities/hall-seat.entity';
import { Program } from 'src/programs/entities/program.entity';
import { Screen } from 'src/screens/entities/screen.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Hall {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @CreateDateColumn()
  createdAt: Date;

  @OneToMany(() => Screen, (screen) => screen.hall)
  screens: Screen[];

  @OneToMany(() => Program, (program) => program.hall)
  programs: Program[];

  @OneToMany(() => HallSeat, (hallSeat) => hallSeat.hall)
  hallSeats: HallSeat[];
}
