/* eslint-disable prettier/prettier */
import { Director } from 'src/directors/entities/director.entity';
import { MovieActor } from 'src/movie-actors/entities/movie-actor.entity';
import { MovieGenre } from 'src/movie-genres/entities/movie-genre.entity';
import { Program } from 'src/programs/entities/program.entity';
import { ShowTime } from 'src/show-times/entities/show-time.entity';
import { Subtitle } from 'src/subtitles/entities/subtitle.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Movie {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '128',
    default: 'no_img.jpg',
  })
  image: string;

  @Column()
  title: string;

  @Column()
  length: number;

  @Column()
  description: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => Director, (director) => director.movies)
  director: Director;

  @Column()
  DirectorId: number;

  @OneToMany(() => ShowTime, (showTime) => showTime.movie)
  showTimes: ShowTime[];

  @OneToMany(() => MovieActor, (movieActor) => movieActor.movie)
  movieActors: MovieActor[];

  @OneToMany(() => Subtitle, (subtitle) => subtitle.movie)
  subtitles: Subtitle[];

  @OneToMany(() => MovieGenre, (movieGenre) => movieGenre.movie)
  movieGenres: MovieGenre[];

  @OneToMany(() => Program, (program) => program.movie)
  programs: Program[];
}
