/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';

export class CreateMovieDto {
  image = 'no_img.jpg';

  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  length: number;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  DirectorId: number;
}
