/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { MoviesController } from './movies.controller';
import { Movie } from './entities/movie.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Director } from 'src/directors/entities/director.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Movie, Director])],
  controllers: [MoviesController],
  providers: [MoviesService],
})
export class MoviesModule {}
