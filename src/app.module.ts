/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { HallsModule } from './halls/halls.module';
import { Hall } from './halls/entities/hall.entity';
import { MoviesModule } from './movies/movies.module';
import { Movie } from './movies/entities/movie.entity';
import { TicketsModule } from './tickets/tickets.module';
import { Ticket } from './tickets/entities/ticket.entity';
import { SeatsModule } from './seats/seats.module';
import { Seat } from './seats/entities/seat.entity';
import { InvoicesModule } from './invoices/invoices.module';
import { Invoice } from './invoices/entities/invoice.entity';
import { Promotion } from './promotions/entities/promotion.entity';
import { ScreensModule } from './screens/screens.module';
import { Screen } from './screens/entities/screen.entity';
import { TimesModule } from './times/times.module';
import { Time } from './times/entities/time.entity';
import { BookingsModule } from './bookings/bookings.module';
import { Booking } from './bookings/entities/booking.entity';
import { DirectorsModule } from './directors/directors.module';
import { OccupiesModule } from './occupies/occupies.module';
import { ShowTimesModule } from './show-times/show-times.module';
import { Director } from './directors/entities/director.entity';
import { Occupy } from './occupies/entities/occupy.entity';
import { ShowTime } from './show-times/entities/show-time.entity';
import { PromotionsModule } from './promotions/promotions.module';
import { ProgramsModule } from './programs/programs.module';
import { MovieActorsModule } from './movie-actors/movie-actors.module';
import { SubtitlesModule } from './subtitles/subtitles.module';
import { MovieGenresModule } from './movie-genres/movie-genres.module';
import { ActorsModule } from './actors/actors.module';
import { LanguagesModule } from './languages/languages.module';
import { GenresModule } from './genres/genres.module';
import { DatesModule } from './dates/dates.module';
import { HallSeatsModule } from './hall-seats/hall-seats.module';
import { Actor } from './actors/entities/actor.entity';
import { Genre } from './genres/entities/genre.entity';
import { HallSeat } from './hall-seats/entities/hall-seat.entity';
import { Language } from './languages/entities/language.entity';
import { MovieActor } from './movie-actors/entities/movie-actor.entity';
import { MovieGenre } from './movie-genres/entities/movie-genre.entity';
import { Program } from './programs/entities/program.entity';
import { Subtitle } from './subtitles/entities/subtitle.entity';
import { Date } from './dates/entities/date.entity';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'DBFuku-data.sqlite',
      synchronize: false,
      entities: [
        User,
        Hall,
        Movie,
        Ticket,
        Seat,
        Invoice,
        Promotion,
        Screen,
        Time,
        Booking,
        Director,
        Occupy,
        ShowTime,
        Actor,
        Date,
        Genre,
        HallSeat,
        Language,
        MovieActor,
        MovieGenre,
        Program,
        Subtitle,
      ],
    }),
    HallsModule,
    UsersModule,
    MoviesModule,
    TicketsModule,
    SeatsModule,
    InvoicesModule,
    PromotionsModule,
    ScreensModule,
    TimesModule,
    BookingsModule,
    DirectorsModule,
    OccupiesModule,
    ShowTimesModule,
    ProgramsModule,
    MovieActorsModule,
    SubtitlesModule,
    MovieGenresModule,
    ActorsModule,
    LanguagesModule,
    GenresModule,
    DatesModule,
    HallSeatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
