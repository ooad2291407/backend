/* eslint-disable prettier/prettier */
import { Invoice } from 'src/invoices/entities/invoice.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Ticket {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  used_status: string;

  @Column()
  qrcode: string;

  @CreateDateColumn()
  createdAt: Date;

  @DeleteDateColumn()
  expired_date: Date;

  @ManyToOne(() => User, (user) => user.tickets)
  user: User;

  @OneToOne(() => Invoice)
  @JoinColumn()
  invoice: Invoice;
}
