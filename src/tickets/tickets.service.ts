/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { Ticket } from './entities/ticket.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TicketsService {
  constructor(
    @InjectRepository(Ticket)
    private ticketsRepository: Repository<Ticket>,
  ) {}

  create(createTicketDto: CreateTicketDto) {
    return this.ticketsRepository.save(createTicketDto);
  }

  findAll() {
    return this.ticketsRepository.find();
  }

  async getTicketByPoint(qrcode: string): Promise<Ticket[]> {
    return this.ticketsRepository.find({ where: { qrcode: qrcode } });
  }

  findOne(id: number) {
    return this.ticketsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateTicketDto: UpdateTicketDto) {
    const ticket = await this.ticketsRepository.findOneBy({ id });
    if (!ticket) {
      throw new NotFoundException();
    }
    const updatedTicket = { ...ticket, ...updateTicketDto };

    return this.ticketsRepository.save(updatedTicket);
  }

  async remove(id: number) {
    const ticket = await this.ticketsRepository.findOneBy({ id });
    if (!ticket) {
      throw new NotFoundException();
    }
    return this.ticketsRepository.softRemove(ticket);
  }
}
